FROM node:17-alpine
WORKDIR /usr/src

COPY package*.json ./
RUN npm install
COPY src/index.js .

EXPOSE 3001
CMD ["npm", "start"]
