module.exports = app => {
    const users = require("../controller/user.controller.js");
    var router = require("express").Router();

    router.post("/register", users.signup);
    router.post("/login", users.login);
    app.use("/api/users", router);
}