const db = require("../model");
const Users = db.users;
const userSecurity = require("../security/user.security");
const jwt = require("jsonwebtoken");
const config = require("../config/auth.config");

exports.signup = (req, res) => {
    const hashedPassword = userSecurity.encrypt(req.body.password);

    const user = new Users({
        username: req.body.username,
        fullname: req.body.description,
        published: req.body.published ? req.body.published : false,
        fulname: req.body.fullname,
        email: req.body.email,
        phoneNumber: req.body.phoneNumber,
        address: req.body.address,
        password: hashedPassword,
        role: req.body.role,
        isDeleted: false,
        isBlocked: false
    });

    user
        .save(user)
        .then(data => {
            delete data.password
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Tutorial."
            });
        });
};

exports.login = async (req, res) => {
    console.log("🚀 ~ exports.login= ~ req:", req.body)
    try {
        const user = Users.findOne({
            username: req.body.username,
        });
        const foundUser = await user.exec();
        if (!foundUser) {
            res.status(404).send({
                message: "Can not find user"
            })
        } else {
            const checkPass = userSecurity.comparePassword(req.body.password, foundUser.password);
            if (checkPass === false) {
                res.status(404).send({
                    message: "Invalid password"
                })
            } else {
                const token = jwt.sign({ id: user.id },
                    config.secret,
                    {
                        algorithm: 'HS256',
                        allowInsecureKeySizes: true,
                        expiresIn: 86400
                    });
                delete foundUser.password
                res.send({ data: foundUser, access_token: token })
            }
        }
    } catch (error) {
        console.log(error)
    }
}