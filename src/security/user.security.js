var CryptoJS = require("crypto-js");
const AES = require('crypto-js/aes');
var env = require('../../.env')


exports.encrypt = (password) => {
    const encrypt = AES.encrypt(password, "abcdf").toString()
    return encrypt
}

exports.decrypt = (password) => {
    const decrypt = CryptoJS.AES.decrypt(password, "abcdf").toString(CryptoJS.enc.Utf8);
    return decrypt
}

exports.comparePassword = (password, decryptedPassword) => {
    return password === this.decrypt(decryptedPassword)
}
