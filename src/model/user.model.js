module.exports = mongoose => {
    var schema = mongoose.Schema(
        {
            username: String,
            fulname: String,
            email: String,
            phoneNumber: String,
            address: String,
            password: String,
            role: String,
            isDeleted: Boolean,
            isBlocked: Boolean
        },
        { timestamps: true }
    );

    schema.method("toJSON", function () {
        const { __v, _id, ...object } = this.toObject();
        object.id = _id;
        return object;
    });

    const Users = mongoose.model("users", schema);
    return Users;
};