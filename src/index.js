const express = require('express');
const app = express()
const port = 3001
const db = require("./model");
const bodyParser = require("body-parser");
const cors = require("cors");

app.get('/', (req, res) => {
    res.send('App is running')
})

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})

app.use(cors())

db.mongoose
    .connect(db.url, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => {
        console.log("Connected to the database!");
    })
    .catch(err => {
        console.log("Cannot connect to the database!", err);
        process.exit();
    });
require("./router/user.router")(app);
